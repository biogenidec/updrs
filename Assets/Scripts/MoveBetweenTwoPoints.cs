﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBetweenTwoPoints : MonoBehaviour
{
    public bool setBool = false; 

    public bool moveBool = false;

    public float distanceThreshold = 0.1f;
    private float diffDistance; 

    public float moveSpeed;
    private float speed; 
    private Vector3 localInit;

    public Vector3 localDiff;
    private Vector3 worldFinal; 

    private GameObject gO;
    private Transform gOT; 
	
    // Use this for initialization
	void Start ()
    {
        gO = this.gameObject;
        gOT = gO.GetComponent<Transform>();
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        speed = moveSpeed * Time.deltaTime;

        if(setBool == true)
        {
            //Update the values for localInit and worldFinal
            localInit = gOT.position;
            worldFinal = localDiff + localInit;
            Debug.Log(worldFinal);
            setBool = false; 
        }


        if (moveBool == true)
        {     
            gOT.position = Vector3.MoveTowards(gOT.position, worldFinal, speed);

            diffDistance = Vector3.Distance(gOT.position, worldFinal);

            //Test to see if object has reached worldFinal 
            if (diffDistance < distanceThreshold)
            {
                //Set moveBool to false
                moveBool = false;
            }
            
            

        }
    }
}
