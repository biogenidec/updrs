﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetChase : MonoBehaviour {

    //Define this gameobject
    private GameObject gO;
    private Transform gOT; 
    private Renderer gOR;
    private Vector3 gOTScale;
    private TrailRenderer gOTR; 

    //Define the chasing objects
    public GameObject rHandObj;
    public GameObject lHandObj;
    private Collider rHandColl;
    private Collider lHandColl;

    //Define timer variables
    public float minTouchTime = 1f;
    private float elapsedTime = 0f;
    //public bool minTouchTimeBool = false;
    public bool timerActiveBool = false;

    //Define color when object is touched
    public Color touchedColor;
    private Color initColor;

    //Movement values
    public float moveSpeed = 1f;
    private float speed;
    public float distanceThreshold = 0.001f;
    private float diffDistance;
    //public bool setBool = false;
    public bool moveBool = false;

    //Define coordinates to move to and related transforms
    public Vector3[] waypointsInLocalCoords;
    private int waypointNumber;
    private Vector3 tempScaledWaypoint; 
    public Vector3[] waypointsInWorldCoords;
    public bool incrWaypoint;
    private int tempDestinationWaypoint = 1;
    //private int coord1 = 1; 

    // Use this for initialization
    void Start ()
    {
        //Define this gameobject 
        gO = this.gameObject;
        gOT = gO.GetComponent<Transform>();
        gOR = gO.GetComponent<Renderer>();
        gOTR = gO.GetComponent<TrailRenderer>();
        gOTScale = gOT.localScale;

        //Debug.Log(gOTScale);

        //Set gO colors
        initColor = gOR.material.color;
        
        if(gOTR != null)
        {
            //Set trailrenderer begin and end gradient colors to initColor and touchedColor, respectively 
            float alpha = 1.0f;
            Gradient gradient = new Gradient();
            gradient.SetKeys(
                new GradientColorKey[] { new GradientColorKey(initColor, 0.0f), new GradientColorKey(touchedColor, 1.0f) },
                new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
                );
            gOTR.colorGradient = gradient; 
        }

        //Define colliders for the chasing objects
        rHandColl = rHandObj.GetComponent<Collider>();
        lHandColl = lHandObj.GetComponent<Collider>();

        //Find initial length of waypointsInLocalCoords
        waypointNumber = waypointsInLocalCoords.Length;

        //Make certain waypointsInLocalCoords 
        if (waypointNumber == 0)
        {
            waypointsInLocalCoords = new Vector3[1];
            waypointNumber = waypointsInLocalCoords.Length;
        }

        //Make certain the first waypoint is (0,0,0) in local coordinates
        waypointsInLocalCoords[0] = Vector3.zero;

        //Define the size of waypointsInWorldCoords to equal waypointsInLocalCoords
        waypointsInWorldCoords = new Vector3[waypointNumber];

        //Set waypointsInWorldCoords to the world coordinates of waypointsInLocalCoords
        for(int i = 0; i < waypointNumber; i++)
        {
            //TransformPoint is impacted by object scale; the following adjusts for the object's scale
            tempScaledWaypoint = new Vector3(waypointsInLocalCoords[i].x / gOTScale.x, waypointsInLocalCoords[i].y / gOTScale.y, waypointsInLocalCoords[i].z / gOTScale.z);

            waypointsInWorldCoords[i] = gOT.TransformPoint(tempScaledWaypoint);
        }

        //Movement
        speed = moveSpeed * Time.deltaTime;
    }

    // Update is called once per frame
    void Update ()
    {

        //Waypoint incrementing
        if(incrWaypoint == true)
        {
            tempDestinationWaypoint += 1; 
            if(tempDestinationWaypoint >= waypointNumber)
            {
                tempDestinationWaypoint = 0; 
            }

            //Debug.Log(tempDestinationWaypoint);

            incrWaypoint = false; 
        }

        //Move to next waypoint
        if(moveBool == true)
        {
            gOT.position = Vector3.MoveTowards(gOT.position, waypointsInWorldCoords[tempDestinationWaypoint], speed);

            diffDistance = Vector3.Distance(gOT.position, waypointsInWorldCoords[tempDestinationWaypoint]);

            //Test to see if object has reached worldFinal 
            if (diffDistance < distanceThreshold)
            {
                //Set moveBool to false
                moveBool = false;
                incrWaypoint = true; 
            }
        }
        
        //Timer 
        if (timerActiveBool == true)
        {
            //Advance elapsedTime
            elapsedTime += Time.deltaTime;
        }

        if (elapsedTime >= minTouchTime)
        {
            //minTouchTimeBool = true;

            //Move to next waypoint
            moveBool = true;

            //Reset timer
            elapsedTime = 0f; 
            timerActiveBool = false; 

        }
    }

    // Called when a collider enters a collider + rigidbody combo
    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collider entered");

        if (other == rHandColl || other == lHandColl)
        {
            Debug.Log("Timer started");
            timerActiveBool = true;

            //Change gO color touched color
            gOR.material.SetColor("_Color", touchedColor);
        }
    }

    // Called when a collider exit a collider + rigidbody combo
    void OnTriggerExit(Collider other)
    {
        Debug.Log("Collider exited");

        if (other == rHandColl || other == lHandColl)
        {
            Debug.Log("Timer ended");
            timerActiveBool = false;
            Debug.Log(elapsedTime);
            elapsedTime = 0f;

            //Change gO color to initial color
            gOR.material.SetColor("_Color", initColor);
        }
    }
}
