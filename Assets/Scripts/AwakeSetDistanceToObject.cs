﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwakeSetDistanceToObject : MonoBehaviour {

    public GameObject anchorObject;
    private Transform aOT;

    public Vector3 distanceBetween;
    private Vector3 startLocation;

    private GameObject gO;
    private Transform gOT;

    // Use this for initialization before Start
    void Awake()
    {
        aOT = anchorObject.GetComponent<Transform>();

        gO = this.gameObject;
        gOT = gO.GetComponent<Transform>();

        startLocation = aOT.position + distanceBetween;
        //Debug.Log(startLocation);

        //Set the gO's position
        gOT.position = new Vector3(startLocation.x, startLocation.y, startLocation.z);
    }
}
