﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectHasTouchedForTime : MonoBehaviour
{
    public GameObject chasingObject;
    private Collider cOColl;

    private GameObject gO;

    public float minTouchTime = 1f; 
    private float elapsedTime = 0f;

    public bool minTouchTimeBool = false;
    private bool timerActiveBool = false; 
    
    // Use this for initialization
	void Start ()
    {
        //gO = this.gameObject;

        cOColl = chasingObject.GetComponent<Collider>(); 
	}

    // Update is called once per frame
    void Update()
    {

        if(timerActiveBool == true)
        {
            elapsedTime += Time.deltaTime;
        }

        if (elapsedTime >= minTouchTime)
        {
            minTouchTimeBool = true; 
        }
    }


    // Called when a collider enters a collider + rigidbody combo
    void OnTriggerEnter (Collider other)
    {
        Debug.Log("Collider entered");

        if(other == cOColl)
        {
            Debug.Log("Timer started");
            timerActiveBool = true;  
        }
        
        
	}

    // Called when a collider exit a collider + rigidbody combo
    void OnTriggerExit(Collider other)
    {
        Debug.Log("Collider entered");

        if (other == cOColl)
        {
            Debug.Log("Timer ended");
            timerActiveBool = false;
            Debug.Log(elapsedTime);
            elapsedTime = 0f;
        }


    }
}
