﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwakeSetColor : MonoBehaviour {

    public Color initialColor; 

    private GameObject gO;
    private Renderer gOR; 
    
    // Use this for initialization before Start
    void Awake()
    {
        gO = this.gameObject;
        gOR = gO.GetComponent<Renderer>();

        gOR.material.SetColor("_Color", initialColor);
    }
}
